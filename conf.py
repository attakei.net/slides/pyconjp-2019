from pathlib import Path

from slides_workspace.default.conf import *  # noqa
from slides_workspace.sass import make_with_revealjs


here = Path(__file__).parent

# -- sphinx-revealjs conf --------------
revealjs_style_theme = "mytheme/main.css"
revealjs_google_font = "Noto Sans JP"
revealjs_script_conf = """
    {
        controls: false,
        progress: true,
        history: true,
        center: true,
        transition: "none",
        width: 1024,
        height: 768,
        maxScale: 1,
    }
"""
revealjs_script_plugins = [
    {
        "src": "revealjs/plugin/notes/notes.js",
        "options": "{async: true}",
    },
    {
        "src": "revealjs/plugin/highlight/highlight.js",
        "options": "{async: true, callback: function() { hljs.initHighlightingOnLoad();}}",
    },
]


def setup(app):
    """
    """
    sass_dir = here / "_sass"
    dest_dir = here / "_static/mytheme"
    make_with_revealjs(sass_dir, dest_dir)
